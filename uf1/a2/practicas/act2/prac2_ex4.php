<!DOCTYPE html>
<html>
    <head>
        <title>Act2 ex4</title>
        <meta charset="UTF-8"/>
    </head>

    <body>
        <?php
            $rgb = rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255);
            echo "rgb($rgb)";
        ?>
        <p style="color: rgb(<?php echo $rgb ?>);"><b> COLOR RGB </b></p>
    </body>
</html>